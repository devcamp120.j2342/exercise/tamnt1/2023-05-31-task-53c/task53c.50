
import model.MovableCircle;
import model.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point = new MovablePoint(3, 4, 1, 1);
        MovableCircle circle = new MovableCircle(5, 2, 3, 2, 2);

        System.out.println(point);
        System.out.println(circle);

        point.moveUp();
        circle.moveRight();

        System.out.println(point);
        System.out.println(circle);
    }
}
